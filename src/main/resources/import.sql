-- https://docs.spring.io/spring-boot/docs/current/reference/html/boot-features-sql.html
-- https://docs.spring.io/spring-boot/docs/current/reference/html/howto-database-initialization.html
-- IN THIS FILE WE CAN WRITE AN SQL SCRIPT CONTAINING:
-- SCHEMA, TABLE AND DATA MANIPULATION QUERIES
-- TO BE EXECUTED AUTOMATICALLY DURING THE INITIALIZATION OF THE APPLICATION
-- AND AFTER THE CREATION OF SCHEMA AND TABLES BY Hibernate
-- IF spring.jpa.hibernate.ddl-auto IS SET TO create OR create-drop
-- IT IS A Hibernate feature (nothing to do with Spring)


INSERT INTO USERS (username, full_name, email, avatar) VALUES ('Elton.Armstrong4', 'Summer Moore', 'Adolf.Jerde59@hotmail.com', '/avatars/summer_moore.jpg');
INSERT INTO USERS (username, full_name, email, avatar) VALUES ('Dolly89', 'Geovanny Stark', 'Martina35@yahoo.com', '/avatars/geovanny_stark.jpg');
INSERT INTO USERS (username, full_name, email, avatar) VALUES ('Monique_Runolfsson86', 'Vada Hand', 'Samantha.Murphy@yahoo.com', '/avatars/vada_hand.jpg');
INSERT INTO USERS (username, full_name, email, avatar) VALUES ('Erick85', 'Justina Dickens', 'Irma41@gmail.com', '/avatars/justina_dickens.jpg');
INSERT INTO USERS (username, full_name, email, avatar) VALUES ('Valentin.Mills49', 'Dr. Maiya Renner', 'Jada71@hotmail.com', '/avatars/maiya_renner.jpg');
INSERT INTO USERS (username, full_name, email, avatar) VALUES ('Cr.Wil', 'Creola Wilkinson', 'creola@hotmail.com', '/avatars/creola_wilkinson.jpg');
INSERT INTO USERS (username, full_name, email, avatar) VALUES ('Jen.Hi', 'Jeniffer Hill', 'Jenhi@gmail.com', '/avatars/jeniffer_hill.jpg');
INSERT INTO USERS (username, full_name, email, avatar) VALUES ('Bob.Tran', 'Bobby Trantow', 'Bob.Sparrow@hotmail.com', '/avatars/bobby_trantow.jpg');
INSERT INTO USERS (username, full_name, email, avatar) VALUES ('Max.Thi', 'Maximillian Thiel', 'Maxi.Thiel@gmail.com', '/avatars/maximillian_thiel.jpg');


INSERT INTO Posts (userId, createdAt, title, body) VALUES ('1', '2020-02-1 12:34:56', 'Personal Loan Account seize Executive', 'Post Body');
INSERT INTO Posts (userId, createdAt, title, body) VALUES ('1', '2020-02-1 12:34:56', 'Personal Loan Account Home Loan Account Car', 'Post Body');
INSERT INTO Posts (userId, createdAt, title, body) VALUES ('1', '2020-02-1 12:34:56', 'Infrastructure car SDD', 'Post Body');
INSERT INTO Posts (userId, createdAt, title, body) VALUES ('1', '2020-02-1 12:34:56', 'Orchestrator', 'Post Body');
INSERT INTO Posts (userId, createdAt, title, body) VALUES ('1', '2020-02-1 12:34:56', 'Hdd invoice reboot', 'Post Body');
INSERT INTO Posts (userId, createdAt, title, body) VALUES ('1', '2020-02-1 12:34:56', 'e-tailers Directives', 'Post Body');

INSERT INTO Comments (postId, userId, createdAt, fullName, avatar, body, email) VALUES(1, 1,'2020-02-1 12:34:56', 'Summer Moore', '/avatars/summer_moore.jpg', 'agree', 'Adolf.Jerde59@hotmail.com');