package com.eurodyn.WebApp.repository;

import com.eurodyn.WebApp.domain.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;
import java.util.Optional;

public interface UsersRepository extends JpaRepository<Users, Long> {

    List<Users> findAll();

    List<Users> findAllById(Long id);

    //Calculate the offset of the data for the page, as well as the number(limit) of items to be displayed
    @Query(value = "SELECT * FROM USERS WHERE  id BETWEEN (:page-1)*(:limit+1) and :page*:limit", nativeQuery = true)
    List<Users> findUsersPagination(@PathVariable("page") int page, @PathVariable("limit") int limit);
}
