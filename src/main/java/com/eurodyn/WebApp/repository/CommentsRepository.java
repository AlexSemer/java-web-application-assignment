package com.eurodyn.WebApp.repository;

import com.eurodyn.WebApp.domain.Comments;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CommentsRepository extends JpaRepository<Comments, Long> {

    List<Comments> findAll();

    List<Comments> findAllByPostId(int postId);
}
