package com.eurodyn.WebApp.repository;

import com.eurodyn.WebApp.domain.Posts;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PostsRepository extends JpaRepository<Posts, Long> {

    List<Posts> findAll();

    List<Posts> findAllByUserId(Long id);

    List<Posts> findAllById(Long id);

}
