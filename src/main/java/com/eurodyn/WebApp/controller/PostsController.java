package com.eurodyn.WebApp.controller;


import com.eurodyn.WebApp.model.PostModel;
import com.eurodyn.WebApp.model.UserModel;
import com.eurodyn.WebApp.service.PostsService;
import com.eurodyn.WebApp.service.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
import static com.eurodyn.WebApp.utils.GlobalAttributes.*;

import java.util.List;

@Controller
public class PostsController {

    @Autowired
    private PostsService postsService;

    @Autowired
    private UsersService usersService;

    //  Json Responses
    @GetMapping(value="/users/{userId}/posts")
    @ResponseBody
    public List<PostModel> showUserPosts(@PathVariable("userId") Long userId) {
        return postsService.findAllByUserId(userId);
    }

    //  View Of page
    @GetMapping(value="/users/{userId}/view")
    public String showUserInfo(Model model, @PathVariable("userId") Long userId) {
        //  Get User
        List<UserModel> user = usersService.findAllById(userId);
        model.addAttribute(USER_INFO, user);

        List<PostModel> posts = postsService.findAllByUserId(userId);
        model.addAttribute(POSTS_LIST, posts);
        return "pages/user_posts";
    }


}
