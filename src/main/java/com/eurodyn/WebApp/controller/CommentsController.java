package com.eurodyn.WebApp.controller;

import com.eurodyn.WebApp.model.CommentModel;
import com.eurodyn.WebApp.model.PostModel;
import com.eurodyn.WebApp.service.CommentsService;
import com.eurodyn.WebApp.service.PostsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
public class CommentsController {

    @Autowired
    private PostsService postsService;

    @Autowired
    private CommentsService commentsService;


    @GetMapping(value="/users/{userId}/posts/{postId}")
    @ResponseBody
    public List<PostModel> showPostInfo(@PathVariable("postId") Long postId ) {
        return postsService.findAllById(postId);
    }

    @GetMapping(value="/users/{userId}/posts/{postId}/comments")
    @ResponseBody
    public List<CommentModel> showPostComments(@PathVariable("postId") int postId ) {
        return commentsService.findAllByPostId(postId);
    }
}
