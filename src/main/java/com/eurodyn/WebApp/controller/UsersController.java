package com.eurodyn.WebApp.controller;

import com.eurodyn.WebApp.model.UserModel;
import com.eurodyn.WebApp.service.PostsService;
import com.eurodyn.WebApp.service.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
import static com.eurodyn.WebApp.utils.GlobalAttributes.*;

import java.util.*;

@Controller
public class UsersController {

    @Autowired
    private UsersService usersService;

    @Autowired
    private PostsService postsService;

    //  Json Responses
    @GetMapping(value="/users/{page}/{limit}")
    @ResponseBody
    public List<Object> showUsers(@PathVariable("page") int page, @PathVariable("limit") int limit) {
        return getUsersResponse(page,limit);  //Return the List of the users
    }

    //  Json User Details (
    @GetMapping(value="/users/{userId}")
    @ResponseBody
    public List<UserModel> showUsersDetails(@PathVariable("userId") Long userId) {
        return usersService.findAllById(userId);  //Return the List of the users
    }

    //  Users View
    @GetMapping(value="/users/{page}/{limit}/view")
    public String showUsersView(Model model, @PathVariable("page") int page, @PathVariable("limit") int limit) {
        List<UserModel> users = usersService.findUsersPagination(page,limit);
        model.addAttribute(USERS_LIST, users);
        return "pages/users_show";
    }

    public List<Object> getUsersResponse(int page, int limit){
        List<Object> response = new ArrayList<>();

        //  Get The <Users> list
        Map<String, List<UserModel>> itemsMap = new HashMap<>();
        List<UserModel> users = usersService.findUsersPagination(page,limit);
        itemsMap.put("items", users);
        //  Get the total size of the list
        Map<String,Integer> totalUsersMap = new HashMap<>();
        totalUsersMap.put("total", users.size());

        //  Add Both objects in the Response
        response.add(itemsMap);
        response.add(totalUsersMap);
        return response;
    }
}
