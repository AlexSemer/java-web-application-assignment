package com.eurodyn.WebApp.service;

import com.eurodyn.WebApp.domain.Posts;
import com.eurodyn.WebApp.model.PostModel;

import java.util.List;

public interface PostsService {

    List<PostModel> findAll();

    List<PostModel> findAllByUserId(Long id);

    List<PostModel> findAllById(Long id);
}
