package com.eurodyn.WebApp.service.implementation;

import com.eurodyn.WebApp.mapper.CommentMapper;
import com.eurodyn.WebApp.model.CommentModel;
import com.eurodyn.WebApp.repository.CommentsRepository;
import com.eurodyn.WebApp.service.CommentsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CommentsServiceImp implements CommentsService {

    @Autowired
    private CommentsRepository commentsRepository;

    @Autowired
    private CommentMapper commentMapper;

    @Override
    public List<CommentModel> findAll() {
        return commentsRepository
                .findAll()
                .stream()
                .map(comment -> commentMapper.mapToCommentModel(comment))
                .collect(Collectors.toList());
    }

    @Override
    public List<CommentModel> findAllByPostId(int postId) {
        return commentsRepository
                .findAllByPostId(postId)
                .stream()
                .map(comment -> commentMapper.mapToCommentModel(comment))
                .collect(Collectors.toList());
    }
}
