package com.eurodyn.WebApp.service.implementation;

import com.eurodyn.WebApp.mapper.UserMapper;
import com.eurodyn.WebApp.model.UserModel;
import com.eurodyn.WebApp.repository.UsersRepository;
import com.eurodyn.WebApp.service.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UsersServiceImp implements UsersService {

    @Autowired
    private UsersRepository usersRepository;

    @Autowired
    private UserMapper userMapper;

    @Override
    public List<UserModel> findAll() {
        return usersRepository
                .findAll()
                .stream()
                .map(user -> userMapper.mapToUserModel(user))
                .collect(Collectors.toList());
    }

    @Override
    public List<UserModel> findAllById(Long id) {
        return usersRepository
                .findAllById(id)
                .stream()
                .map(user -> userMapper.mapToUserModel(user))
                .collect(Collectors.toList());
    }

    @Override
    public List<UserModel> findUsersPagination(int page, int limit) {
        return usersRepository
                .findUsersPagination(page,limit)
                .stream()
                .map(user -> userMapper.mapToUserModel(user))
                .collect(Collectors.toList());
    }
}
