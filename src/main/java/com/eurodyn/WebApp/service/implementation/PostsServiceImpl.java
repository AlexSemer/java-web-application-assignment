package com.eurodyn.WebApp.service.implementation;

import com.eurodyn.WebApp.mapper.PostMapper;
import com.eurodyn.WebApp.model.PostModel;
import com.eurodyn.WebApp.service.PostsService;
import com.eurodyn.WebApp.repository.PostsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class PostsServiceImpl implements PostsService {

    @Autowired
    private PostsRepository postsRepository;

    @Autowired
    private PostMapper postMapper;

    @Override
    public List<PostModel> findAll() {
        return postsRepository
                .findAll()
                .stream()
                .map(post -> postMapper.mapToPostModel(post))
                .collect(Collectors.toList());
    }

    @Override
    public List<PostModel> findAllByUserId(Long id) {
        return postsRepository
                .findAllByUserId(id)
                .stream()
                .map(post -> postMapper.mapToPostModel(post))
                .collect(Collectors.toList());

    }

    @Override
    public List<PostModel> findAllById(Long id) {
        return postsRepository
                .findAllById(id)
                .stream()
                .map(post -> postMapper.mapToPostModel(post))
                .collect(Collectors.toList());
    }
}
