package com.eurodyn.WebApp.service;

import com.eurodyn.WebApp.model.CommentModel;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface CommentsService {

    List<CommentModel> findAll();

    List<CommentModel> findAllByPostId(int postId);
}
