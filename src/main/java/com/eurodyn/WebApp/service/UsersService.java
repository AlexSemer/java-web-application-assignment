package com.eurodyn.WebApp.service;

import com.eurodyn.WebApp.model.UserModel;

import java.util.List;

public interface UsersService {

    List<UserModel> findAll();

    List<UserModel> findAllById(Long id);

    List<UserModel> findUsersPagination(int page, int limit);
}
