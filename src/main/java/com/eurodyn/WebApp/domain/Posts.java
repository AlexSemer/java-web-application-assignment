package com.eurodyn.WebApp.domain;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "POSTS")
public class Posts {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "userId")
    private Long userId;

    @Column(name = "createdAt")
    private Date createdAt;

    @Column(name = "title")
    private String title;

    @Column(name = "body")
    private String body;

    public Posts() {
    }

    public Posts(Long userId, Date createdAt, String title, String body) {
        this.userId = userId;
        this.createdAt = createdAt;
        this.title = title;
        this.body = body;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }
}
