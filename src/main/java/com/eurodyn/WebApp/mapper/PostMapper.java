package com.eurodyn.WebApp.mapper;

import com.eurodyn.WebApp.domain.Posts;
import com.eurodyn.WebApp.model.PostModel;
import org.springframework.stereotype.Component;

@Component
public class PostMapper {

    public PostModel mapToPostModel(Posts post){
        PostModel postModel = new PostModel();
        postModel.setId(post.getId());
        postModel.setUserId(post.getUserId());
        postModel.setCreatedAt(post.getCreatedAt());
        postModel.setTitle(post.getTitle());
        postModel.setBody(post.getBody());
        return postModel;
    }
}
