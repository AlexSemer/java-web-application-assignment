package com.eurodyn.WebApp.mapper;

import com.eurodyn.WebApp.domain.Users;
import com.eurodyn.WebApp.model.UserModel;
import org.springframework.stereotype.Component;

@Component
public class UserMapper {

    public UserModel mapToUserModel(Users user){
        UserModel userModel = new UserModel();
        userModel.setId(user.getId());
        userModel.setUsername(user.getUsername());
        userModel.setFullName(user.getFullName());
        userModel.setEmail(user.getEmail());
        userModel.setAvatar(user.getAvatar());
        return userModel;
    }
}
