package com.eurodyn.WebApp.mapper;

import com.eurodyn.WebApp.domain.Comments;
import com.eurodyn.WebApp.model.CommentModel;
import org.springframework.stereotype.Component;

@Component
public class CommentMapper {

    public CommentModel mapToCommentModel(Comments comment){
        CommentModel commentModel = new CommentModel();
        commentModel.setId(comment.getId());
        commentModel.setPostId(comment.getPostId());
        commentModel.setUserId(comment.getUserId());
        commentModel.setCreatedAt(comment.getCreatedAt());
        commentModel.setEmail(comment.getEmail());
        commentModel.setAvatar(comment.getAvatar());
        commentModel.setFullName(comment.getFullName());
        commentModel.setBody(comment.getBody());
        return commentModel;
    }

}
