package com.eurodyn.WebApp.utils;


public class GlobalAttributes {

    public static final String USERS_LIST = "users";
    public static final String USER_INFO = "user";

    public static final String POSTS_LIST = "posts";

    public static final String COMMENTS_LIST = "comments";

}
